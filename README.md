# PortMaster scripts

This repository contains build & packaging scripts for some of PortMaster games. The idea is to share information on the build process, so that anyone could reproduce it.

There are three ways to run the scripts:
## Dedicated chroot
This method is simple if you use Linux and is recommended if you want to keep working with the game source code.
The process of creating a chroot is described [here](https://github.com/christianhaitian/rk3326_core_builds/blob/rk3326/docs/chroot.md) or [here](https://github.com/Cebion/Portmaster_builds).
When the chroot is ready, run `./build.sh` from a port directory. It will create a .zip file in the same directory.

## Docker
This method should work on all operating systems, but so far has been tested only on Linux. It might be the quickest one, if you are interested in simply rebuilding a port package.
You need to have docker-buildx installed (see the [instructions](Requires docker-buildx https://docs.docker.com/build/install-buildx/)).
In order to build a port, simply run `./docker_build.sh <port>` in the top level directory. Once the build is completed, you will find a .zip file in the port directory.

If you get `exec /bin/bash: exec format error` message, run first `docker run --rm --privileged multiarch/qemu-user-static:register` and try again.

## Virtual machine
There are [virtual machine images](https://forum.odroid.com/viewtopic.php?p=306185#p306185), preconfigured for building ARM binaries. I have not tested this way myself and most likely it will be the slowest one. Please let me know if it works for you.
