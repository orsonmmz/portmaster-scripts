#!/bin/bash
if [ "$1" == "" ]; then
    echo "usage: $0 <port name>"
    exit
fi

if [ ! -d "$1" ]; then
    echo "invalid port name"
    exit
fi

docker run -it -v ./$1:/build multiarch/debian-debootstrap:arm64-buster sh -c "cd /build; sh ./build.sh"
