#!/bin/bash
set -e

# Building
apt-get update && apt-get install -y clang-13 cmake git libsdl2-dev zip
git clone https://github.com/alexbatalov/fallout2-ce src && mkdir src/build
cd src/build
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=clang-13 -DCMAKE_CXX_COMPILER=clang++-13 ..
make

# Packaging
cd /build
cp src/build/fallout2-ce fallout2/fallout2-ce
strip fallout2/fallout2-ce
zip -r9 Fallout2.zip "Fallout 2.sh" fallout2
